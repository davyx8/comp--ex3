function [ im ] = focus( imgs,fcs, trans, med)
% get set of images and value - val and return image with focus that
% changed according to val argument
len = length(imgs) ; 
half = ceil(len/2);
imgWidth = size(imgs{1},2);
maxTrans = trans*len;
tmp = half-len:len-half;
im = zeros(size(imgs{1}));
[h, w,q] = size(imgs{2});
arr = zeros(len, h, w, 3);
for i=1:len
    currfcs =  tmp(i)*fcs;
    if ceil(currfcs) == floor(currfcs)
        im2 = circshift(imgs{i},[0,currfcs]);
      
    else
        upper = ceil(currfcs);
        lower = floor(currfcs);
        frc = (upper - currfcs);
        im2 = circshift(imgs{i},[0,upper])*frc + (1-frc)*circshift(imgs{i},[0,lower]);
    end
    arr(i,:,:,:)=im2;   
    im = im + im2;
end
if (med==1)
tot = median(arr);
size(tot)
tot = reshape(tot, h, w, 3); 
im = tot;
else
im = im ./ len;
end
im = im(:,maxTrans:imgWidth,:);
end

