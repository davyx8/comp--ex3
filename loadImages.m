function [ imgs ] = loadImages( foldername , imgType )
%LOADIMAGES Summary of this function goes here
%   Detailed explanation goes here

Imgs = dir([foldername '/']);
    NumImgs = size(Imgs,1);
    image = double(imread([foldername '/' Imgs(3).name]));
    X = zeros([NumImgs size(image)]);
    for i=3:NumImgs,
      image = im2double(imread([foldername '/' Imgs(i).name]));
      imgs{i-2}=image;
    end
end

    