function [ finalImage ] = POV( imgs, X,Y , trans )
%ROTATE - this fuction creates a rotation on the image;
% imgs - the set of images.
% X,Y is the location of the POV, where X=0 is left corner, and Y=0 is
% farthest from image. X,Y are [0,1];

% to find the exact part of the images we need to figure the width of the
% resulting image, depending on the Y axis (assuming we view 45 deg to each
% side) using tangents:

% num of photos
n = length(imgs);
%% single image widht:
imgWidth= size(imgs{1},2);
imgHeight= size(imgs{1},1);
% stripWidth = width/n;
zoom = (Y+0.5)
stripWidth = ceil(trans*zoom);

X=X*imgWidth;
Ytop = imgHeight;
yBottom = 1;
imgHeightEdit =  imgHeight;
% end;
% initiate final image:
finalImage = zeros([imgHeightEdit,stripWidth*n,3]);

leftX = X-stripWidth/2+1;
rightX = X+stripWidth/2;
finalImgIdx = max(ceil(X-stripWidth/2+1),1);
finalImgIdx1 = max(ceil(X-stripWidth/2+1),1);
% stX = finalImgIdx-(stripWidth/2)+1;
% figure;
move = trans;
for i=1:n
    
        if(i==1)
            shift = 0;
        else
            shift = move;
        end
%         leftIdx = leftX+stripWidth*(i-1)-shift*(i-1);
%         rightIdx = leftX+stripWidth*(i)-shift*(i-1);
        leftIdx = leftX;
        rightIdx = rightX;
        leftIdx = max(ceil(leftIdx),1);
        if(leftIdx>=imgWidth)
            continue;
        end;
        rightIdx = min(floor(rightIdx),imgWidth);
        currWidth = rightIdx-leftIdx;
        if(rightIdx<=1)
            continue;
        end;
        finalImage(:,finalImgIdx:finalImgIdx+currWidth,:) = imgs{i}(yBottom:Ytop,(leftIdx):(rightIdx),:);
%                imshow(finalImage);

        finalImgIdx = finalImgIdx+currWidth;
end
        finalImage = finalImage(:,finalImgIdx1:finalImgIdx,:);
%         
% figure;
% imshow(finalImage);


end

